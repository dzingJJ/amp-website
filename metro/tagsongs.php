<?php session_start();
include 'auth.php';
if(isset($_SESSION['logged']) && $_SESSION['logged'] === true) {
    $opts = [
        "http" => [
            "method" => "GET",
            "header" => "Auth-Key: " . $_SESSION['authkey'] . "\r\n"
        ]
    ];
    $context = stream_context_create($opts);
    $file = file_get_contents($_SESSION['host'] . "songs/accept_list", false, $context);
    $response = json_decode($file, true);

    if (!isset($response['status'])) {
        $num = count($response['json']);
        echo '<script type="text/javascript" src="scripts/songaccept.js"></script>';
        for ($i = 0; $i < $num; $i++) {
            echo '<form id="accl" class="acceptlist" name="' . $response['json'][$i]['id'] . '" onSubmit="accept(this.name, this.elements[\'title\'].value, this.elements[\'author\'].value); return false;">' .
                'File: ' . $response['json'][$i]['fullTitle'] . '<br/>' .
                'Title: <input type="text" name="title" value="' . $response['json'][$i]['title'] . '" /><br/>' .
                'Author: <input type="text" name="author" value="' . $response['json'][$i]['author'] . '" /><br/>' .
                '<input type="submit" value="Accept" /> <a target="_blank" href="'.$response['json'][$i]['url'].'"><button type="button" value="Check">Check</button></a>' .
                '</form><br/>';
        };
    } else {
        echo 'You don\'t have permission or there is an external server error';
    };
} else {
    echo 'You need to be logged in in order to tag songs';
};
?>
