<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<meta charset="UTF-8">
<link rel="shortcut icon" href="favicon.png">
<link rel="stylesheet" type="text/css" href="css/default.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
<title><?php echo $title.' | Metro Kopernik - Radio II LO Leszno'; ?></title>
<?php
// @param server host host in the following syntax:
// 'http://serveraddress/'77.254.48.81
 $_SESSION['host'] = 'http://localhost:8080/';
if (isset($_COOKIE['login']) && isset($_COOKIE['pass']) && (!isset($_SESSION['logged']) || $_SESSION['logged'] !== true)){
    ?><script type="text/javascript">window.location.replace('login.php?cookie=y&address='+window.location.href);</script><?php
};
?>
