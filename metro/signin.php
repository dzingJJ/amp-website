<?php session_start();
$name = $_POST['rname'];
$surname = $_POST['rsurname'];
$email = $_POST['remail'];
$login = $_POST['rlogin'];
$pass = $_POST['rpass'];
$passconfirm = $_POST['rpassconfirm'];
if ($name == "" || $surname == "" || $email == "" || $login == "" || $pass == "" || $passconfirm == ""){
	$_SESSION['error'] = "all fields must be filled";
	echo '<script>window.location.replace("signin.php");</script>';
}
if ($pass != $passconfirm){
	$_SESSION['error'] = "passwords don't match";
	echo '<script>window.location.replace("signin.php");</script>';
}
include('Crypt/RSA.php');
//$key = json_decode(file_get_contents('http://10.0.0.60:8080/login/public_key_request'), true)['json'];

/*
$rsa = new Crypt_RSA();
$rsa->loadKey($key);
$rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
$encpass = $rsa->encrypt($pass);
*/
$ch = curl_init($_SESSION['host'].'login/register');
# Setup request to send json via POST.
$payload = json_encode( 
	array( 
		"name"=> $name,
		"surname" => $surname,
		"email" => $email,
		"username" => $login,
		"passKey" => $pass
		 ) );
curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
# Return response instead of printing.
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

# Send request.
$result = curl_exec($ch);
curl_close($ch);

// further processing ....
//if ($server_output == "OK") { ... } else { ... }

//file_get_contents('http://10.0.0.60:8080/system?passkey='.$encpass);
/*
if($server_output['response'] == 'ok'){
	$_SESSION['logged'] = true;
	$_SESSION['user'] = $login;
	$_SESSION['authKey'] = $server_output['additionalData'][0];
	if(isset($_SESSION['error'])){
		$_SESSION['error'] = "";
		unset($_SESSION['error']);
	};
} else {
	$_SESSION['logged'] = false;
	if($server_output['response'] == 'user_not_found'){
	$_SESSION['error'] = "No such user";
};
	if($server_output['response'] == 'invalid_password'){
		$_SESSION['error'] = "Invalid password";
	}
}
*/
?>
<script type="text/javascript">window.location.replace("user.php")</script>