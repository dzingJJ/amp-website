<?php session_start();
include 'auth.php';
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $_SESSION['host']."login/logout");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Auth-Key: '.$_SESSION['authkey']]);

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS,
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = json_decode(curl_exec ($ch), true);

curl_close ($ch);
session_destroy();
setcookie("login", "", time() - 3600);
setcookie("pass", "", time() - 3600);
?>
<script type="text/javascript">window.location.replace("user.php")</script>