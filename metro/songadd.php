<?php session_start();
if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {
	include 'auth.php';
	$ch = curl_init();

$id = $_GET['id'];

curl_setopt($ch, CURLOPT_URL, $_SESSION['host']."songs/schedule");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Auth-Key: '.$_SESSION['authkey'], 'Content-Type: application/x-www-form-urlencoded']);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "id=$id");

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = json_decode(curl_exec ($ch), true);

curl_close ($ch);

$song = json_decode(file_get_contents($_SESSION['host']."public/song?songId=".$id), true);
echo 'Added <b>'.$song['json']['artist']." - ".$song['json']['title']."</b> to the current queue";
} else {
	echo 'You have to be logged in in order to add a song to queue';
};
?>