<?php session_start();
$f = file_get_contents($_SESSION['host']."public/song?songId=".$_GET['id']);
$info = json_decode($f, true);
if($info['response'] != "song_not_found"){
echo '<b>Artist:</b> '.$info['json']['artist'].'<br/>';
echo '<b>Title:</b> '.$info['json']['title'].'<br/>';
if ($info['json']['creator'] == "file_loaded") {
	echo '<b>Added by:</b> server<br/>';
} else{
echo '<b>Added by:</b> '.$info['json']['creator'].'<br/>';
};
$min = floor($info['json']['duration']/60);
$sec = $info['json']['duration'] - ($min*60);
if($sec < 10){
	$sec = "0".$sec;
};
$time = $min.":".$sec;
echo '<b>Duration:</b> '.$time.'<br/>';
} else {
	echo 'Song not found!'.'<br/>';
};
?>