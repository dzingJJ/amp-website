<?php session_start();
include 'auth.php';
$ch = curl_init();

$vol = $_GET['vol'];

curl_setopt($ch, CURLOPT_URL, $_SESSION['host']."songs/volume");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Auth-Key: '.$_SESSION['authkey'], 'Content-Type: application/x-www-form-urlencoded']);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "volume=$vol");

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = json_decode(curl_exec ($ch), true);

curl_close ($ch);

echo $vol;
?>