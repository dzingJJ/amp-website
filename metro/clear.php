<?php session_start();
$ch = curl_init();
require 'auth.php';

curl_setopt($ch, CURLOPT_URL, $_SESSION['host']."songs/clear_playlist");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Auth-Key: '.$_SESSION['authkey']]);
curl_setopt($ch, CURLOPT_POSTFIELDS, ['stop='.$_GET['stop']]);

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = json_decode(curl_exec ($ch), true);

curl_close ($ch);

if (isset($server_output['status'])) {
	echo "no permission";
} else {
echo "ok";
};
?>