<?php session_start();
include 'auth.php';

$ch = curl_init();

$url = urlencode($_GET['url']);

curl_setopt($ch, CURLOPT_URL, $_SESSION['host']."songs/download");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Auth-Key: '.$_SESSION['authkey'], 'Content-Type: application/x-www-form-urlencoded']);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "url=$url");

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = json_decode(curl_exec ($ch), true);

curl_close ($ch);

if (isset($server_output['status'])) {
	echo 'You don\'t have permission to download songs';
	} else if ($server_output['response'] == 'error') {
		echo 'Server error, couldn\t download';
	} else {
		echo "Succesfully downloaded song to the server";
	};
?>