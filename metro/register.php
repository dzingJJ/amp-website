<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<?php require('head.php'); ?>
	<title>User's panel - Automatic Music Player II LO Leszno</title>
</head>
<body>
	<div class="row" id="title">
		<div class="col-12"><h1 id="page">User's panel</h1></div>
		<?php require('menu.php'); ?>
	</div>
<div class="row" id="body">
	<div class="col-3">
	<?php if(!isset($_SESSION['logged']) || $_SESSION['logged'] == false){
		echo '<form action="signin.php" method="post" class="login">
			Name:<br/><input type="text" name="rname"><br/>
			Surname:<br/><input type="text" name="rsurname"><br/>
			Email:<br/><input type="text" name="remail"><br/>
			Login:<br/><input type="text" name="rlogin"><br/>
			Password:<br/><input type="password" name="rpass"><br/>
			Confirm password:<br/><input type="password" name="rpassconfirm"><br/>
			<small>Have an account? <a href="login.php">Log in</a></small><br/>
			<input type="submit" name="rsubmit" value="Sign in">
		</form>';
		if(isset($_SESSION['error']) && $_SESSION['error'] != ""){
			echo "<br/>".$_SESSION['error']."<br/>";
			$_SESSION['error'] = "";
			unset($_SESSION['error']);
		};
	} else {
		echo 'Welcome!<br/>
		<form action="logout.php" method="GET">
		<input type="submit" value="Log out">
		</form>
		';
	}; 
		?>
	</div>
</div>
</body>
<footer><?php include 'footer.php'; ?></footer>
</html>