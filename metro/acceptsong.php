<?php session_start();
include 'auth.php';
$ch = curl_init();

$id = $_GET['id'];
$title = $_GET['title'];
$author = $_GET['author'];

curl_setopt($ch, CURLOPT_URL, $_SESSION['host']."songs/accept_and_tag");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Auth-Key: '.$_SESSION['authkey'], 'Content-Type: application/x-www-form-urlencoded']);
curl_setopt($ch, CURLOPT_POSTFIELDS,"id=$id&title=$title&author=$author");

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS,
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = json_decode(curl_exec ($ch), true);

if ($server_output['response'] === "ok"){
    echo 'Successfully added <b>'.$author."</b> - <b>".$title."</b> to the list of songs";
} elseif ($server_output['response'] === "no_tags"){
    echo 'Not all tags have been provided';
} elseif ($server_output['response'] === "not_found"){
    echo 'no such song in accept list';
} else {
    echo 'External server error';
};

curl_close ($ch);
?>