<?php session_start();
$data = file_get_contents($_SESSION['host']."public/current_song");
$decode = json_decode($data, true);
if ($decode['response'] == 'currently_no_playing'){
	echo "Currently not playing any song";
} else {
	echo $decode['json']['artist']." - ".$decode['json']['title']." | <span id='songtime'>".$decode['additionalData'][1]."</span>/<span id='songend'>".$decode['json']['duration'];
};
?>