<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<?php 
	$title = 'Control panel';
	require('head.php'); 
	?>
	<!--<script type="text/javascript" src="scripts/clock.js"></script>-->
	<script type="text/javascript" src="scripts/songinfo.js"></script>
	<script type="text/javascript" src="scripts/songreq.js"></script>
	<script type="text/javascript" src="scripts/songadd.js"></script>
	<script type="text/javascript" src="scripts/volumeslider.js"></script>
	<script type="text/javascript" src="scripts/downloadsong.js"></script>
    <script type="text/javascript" src="scripts/tagsongs.js"></script>
    <script type="text/javascript" src="scripts/songaccept.js"></script>
    <script>$("#accl").submit(function () {
            alert("ID!");
        });</script>
</head>
<body>
<div class="row" id="title">
	<div class="col-12"><h1 id="page">CONTROL PANEL</h1></div>
	<?php require('menu.php'); ?>
	</div>
	<div class="row" id="body">
	<div class="row">
		<div class="col-3 col12">
			<div class="col-12 volumeplayer">
				<?php if(isset($_SESSION['logged']) && $_SESSION['logged'] == true) {
					echo '<input type="range" min="1" max="100" value="1" class="slider" name="slider" id="volumeslider" oninput="slider(this.value)" onchange="volumechange(this.value)" /> <span id="volume">Volume: &nbsp&nbsp%</span>';
				} else { 
					echo 'You need to be logged in in order to change the volume';
				}; ?>
			 </div>
            <?php if (isset($_SESSION['logged']) && $_SESSION['logged'] === true) {
                echo '<img src = "img/stop.png" alt = "stop" title = "Stop music" class="music left" id = "stopbutton" >
			          <img src = "img/play.png" alt = "play/next" title = "Start queue or play next" class="music right" id = "playbutton" >
                ';}; ?>
		</div>
		<div class="col-6 col6">
			Show songs 
			<select id="sort">
				<option value="author">by author</option>
				<option value="title">by title</option>
			</select><br/><br/>
			<div id="response"></div>
		</div>
		<div class="col-3 col6"><div id="anotherresponse">
			Click on a question mark (<b>?</b>) to see detailed info about a specific song.
			Click on a plus (<b>+</b>) to add a song to the current queue <?php //echo $_SESSION['authkey']; ?>
		</div></div>
	</div>
	<div class="row col-12">
		<?php if(isset($_SESSION['logged']) && $_SESSION['logged'] == true){
			echo '<div class="col-12 downloadsong">
			<form id="downloadsong" onsubmit="return download(document.getElementById(\'youtube\').value)">
				Song\'s address (youtube): <input type="text" name="youtube" id="youtube" /> <input type="submit" name="submit" value="Add song" />
			</form>
		</div>';
		} else {
			echo 'You have to be logged in in order to try to download a song';
		};
		?>
	</div>
	<div class="row col-12">
        <div class="col-12" id="tagsongs"></div>
    </div>
	<!-- END -->
</div>
</body>
<footer><?php include 'footer.php'; ?></footer>
</html>