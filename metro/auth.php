<?php
if(isset($_SESSION['logged']) && $_SESSION['logged'] == true){
$opts = [
    "http" => [
        "method" => "GET",
        "header" => "Auth-Key: ".$_SESSION['authkey']."\r\n"
    ]
];
$context = stream_context_create($opts);
$file = file_get_contents($_SESSION['host']."login/auth_check", false, $context);
$resp = json_decode($file, true);
	if (!$resp['json']){
	$c = curl_init();

curl_setopt($c, CURLOPT_URL, $_SESSION['host']."login/login");
curl_setopt($c, CURLOPT_POST, 1);
curl_setopt($c, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
curl_setopt($c, CURLOPT_POSTFIELDS,
            "username=".$_SESSION['user']."&passKey=".$_SESSION['pass']);

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

$server_output = json_decode(curl_exec ($c), true);

curl_close ($c);

$_SESSION['authkey'] = $server_output['additionalData'][0];
};
};
?>