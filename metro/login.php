<?php session_start();
//include('Crypt/RSA.php');
if (isset($_GET['cookie']) && $_GET['cookie'] === "y"){
    $_POST['login'] = $_COOKIE['login'];
    $_POST['pass'] = $_COOKIE['pass'];
};
//$key = json_decode(file_get_contents('http://10.0.0.60:8080/login/public_key_request'), true)['json'];
$login = $_POST['login'];
$pass = $_POST['pass'];

/*
$rsa = new Crypt_RSA();
$rsa->loadKey($key);
$rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
$encpass = $rsa->encrypt($pass);
*/
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $_SESSION['host']."login/login");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "username=$login&passKey=$pass");

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = json_decode(curl_exec ($ch), true);

curl_close ($ch);

// further processing ....
//if ($server_output == "OK") { ... } else { ... }

//file_get_contents('http://10.0.0.60:8080/system?passkey='.$encpass);

if($server_output['response'] == 'ok'){
	$_SESSION['logged'] = true;
	$_SESSION['user'] = $login;
	$_SESSION['pass'] = $pass;
	$_SESSION['authkey'] = $server_output['additionalData'][0];
	setcookie("login", $login, time() + (86400 * 30));
	setcookie("pass", $pass, time() + (86400 * 30));
	if(isset($_SESSION['error'])){
		$_SESSION['error'] = "";
		unset($_SESSION['error']);
	};
} else {
    $_SESSION['logged'] = false;
    if ($server_output['response'] == 'user_not_found') {
        $_SESSION['error'] = "No such user";
    };
    if ($server_output['response'] == 'invalid_password') {
        $_SESSION['error'] = "Invalid password";
    };
};
	if (isset($_GET['address'])){
	    header("Location: ".$_GET['address'], true, 301);
        echo '<div id=\'address\'>'.$_GET['address'].'</div>';
	    ?><script type="text/javascript">window.location.replace(document.getElementById('address').innerHTML);</script><?php
        exit;
    } else {
	    header("Location: user.php", true, 301);
	    ?><script type="text/javascript">window.location.replace("user.php");</script><?php
        exit;
};
?>