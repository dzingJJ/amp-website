<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<?php
	$title = 'Contact';
	 require('head.php'); 
	 ?>
	<title>Contact - Automatic Music Player II LO Leszno</title>
</head>
<body>
<div class="row" id="title">
	<div class="col-12"><h1 id="page">Contact</h1></div>
	<?php require('menu.php'); ?>
	</div>
</div>
<div class="row" id="body">
    <div class="col-3 col6">
        <h2>Where to find us?</h2>
        <p>We can be found on some breaks in <b>P0</b>, and if you don't catch us, write on <b>one of the following contact sources</b>
            or send us a mail through our <b>mailbox</b> in the <b>main hall</b>.</p>
    </div>
</div>
</body>
<footer><?php include 'footer.php'; ?></footer>
</html>