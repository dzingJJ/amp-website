<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<?php
	$title = 'User\'s panel'; 
	require('head.php'); 
	?>
</head>
<body>
<div class="row" id="title">
	<div class="col-12"><h1 id="page">User's panel</h1></div>
	<?php require('menu.php'); ?>
	</div>
	<!-- body -->
	<div class="row" id="body">
		<div class="col-3">
		<?php if(!isset($_SESSION['logged']) || $_SESSION['logged'] == false){
		echo '<form action="login.php" method="post" class="login">
			Login:<br/><input type="text" name="login"><br/>
			Password:<br/><input type="password" name="pass"><br/>
			<small>Don\'t have an account? <a href="register.php">Sign in</a></small><br/>
			<input type="submit" name="submit" value="Log in">
		</form>';
		if(isset($_SESSION['error']) && $_SESSION['error'] != ""){
			echo "<br/>".$_SESSION['error']."<br/>";
			$_SESSION['error'] = "";
			unset($_SESSION['error']);
		};
	} else {
		echo $_SESSION['authkey'].'<br/>
		<form action="logout.php" method="GET">
		<input type="submit" value="Log out">
		</form>
		';
	}; 
		?>
	</div>
	</div>
	<!-- end -->
</div>
</body>
<footer><?php include 'footer.php'; ?></footer>
</html>