<?php session_start();
include 'auth.php';
$opts = [
    "http" => [
        "method" => "GET",
        "header" => "Auth-Key: ".$_SESSION['authkey']."\r\n"
    ]
];
$context = stream_context_create($opts);
$file = file_get_contents($_SESSION['host']."songs/volume", false, $context);
$vol = json_decode($file, true)['json'];
if ($vol < 0) {
	$vol = 0;
};
if ($vol > 100) {
	$vol = 100;
};
echo $vol;
?>