<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<?php
	$title = 'Homepage'; 
	require('head.php'); 
	?>
	<script type="text/javascript" src="scripts/playlist.js"></script>
	<script type="text/javascript" src="scripts/clock.js"></script>
	<script type="text/javascript" src="scripts/songreq.js"></script>
	<script type="text/javascript" src="scripts/clearplaylist.js"></script>
</head>
<body onload="s()">
<div class="row" id="title">
	<div class="col-12"><h1 id="page">Homepage</h1></div>
	<?php require('menu.php'); ?>
	</div>
</div>
<div class="row" id="body">
	<div class="row">
	<div class="col-3 col12" id="clock">
		0:00:00
	</div>
	<div class="col-6 col6" id="playlist"></div>
	<div class="col-3 col6" id="anotherresponse">Click on a question mark (?) to see detailed info about a specific song</div>
</div>
	<!-- END -->
</div>
</body>
<footer><?php include 'footer.php'; ?></footer>
</html>
