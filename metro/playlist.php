<?php session_start();

echo '<b>Songs in current queue</b> <button type="button" id="clear" onClick="clearplaylist()"><b>clear</b></button> <input type="checkbox" id="doStop"/> <small>and stop music</small><br/><br/>';

	$data = file_get_contents($_SESSION['host']."public/playlist");

$decode = json_decode($data, true);
if ($decode['json'] == []) {
	echo 'There are no scheduled songs';
} else {
	$count = count($decode['json']);
	for ($i=0; $i < $count; $i++) { 
		if($decode['json'][$i]['scheduler'] == "guest"){
			$scheduler = 'server';
		} else {
			$scheduler = $decode['json'][$i]['scheduler'];
		};
		echo '<button type="button" name="'.$decode['json'][$i]['songId'].'" class="info" onClick="clicked(this.name)"><b>?</b></button> '.$decode['json'][$i]['songName'].' (scheduled by <i>'.$scheduler.'</i>)<br/>';
	};
};
?>